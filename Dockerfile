FROM openjdk:16-alpine3.13

    #ARG JAR_FILE=target/*.jar
    #COPY ${JAR_FILE} user-foundation.jar
    #ENTRYPOINT ["java","-jar","/user-foundation.jar"]
COPY target/user-foundation-0.0.1-SNAPSHOT.jar user-foundation-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/user-foundation-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080
