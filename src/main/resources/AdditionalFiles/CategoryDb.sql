#CREATE DATABASE category_database;
USE category_database;
CREATE TABLE category(
    category_id BIGINT NOT NULL ,
    name varchar(40),
    deleted boolean NOT NULL ,
    PRIMARY KEY (category_id)
);
INSERT INTO category(category_id, name,deleted) VALUES (1,'Furniture',false);