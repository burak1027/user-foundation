#CREATE DATABASE comment_database;
USE comment_database;
CREATE TABLE comment(
    comment_id bigint NOT NULL ,
    post_id bigint NOT NULL ,
    comment_user_id bigint NOT NULL ,
    replied_comment_id bigint NOT NULL ,
    date_created DATETIME NOT NULL ,
    date_updated DATETIME NOT NULL ,
    content TEXT NOT NULL,
    deleted boolean NOT NULL ,
    PRIMARY KEY (comment_id)

);
INSERT INTO comment (comment_id, post_id, comment_user_id, replied_comment_id, date_created, date_updated, content,deleted)
VALUES(1,1,1,1,'2021-01-01','2021-01-01','It was really fun',false);
