#CREATE DATABASE UserDatabase;
USE user_database;

CREATE TABLE user(
     Tc_No BIGINT NOT NULL ,
    Name varchar(200) NOT NULL,
    surname varchar(200) NOT NULL,
    user_name varchar(200) NOT NULL ,
    Gender varchar(32) NOT NULL ,
    birth_place varchar(32) NOT NULL ,
    date_of_birth  DATE NOT NULL,
    deleted boolean NOT NULL ,
    PRIMARY KEY (Tc_No),
    UNIQUE (user_name)

);
INSERT INTO user(Tc_No, Name, surname,user_name, Gender, birth_place, date_of_birth, deleted)
 values (46312956754,'Burak','Gunden','bgunden','Male','Istanbul','1999-10-05',false);
INSERT INTO user(Tc_No, Name, surname, user_name,Gender, birth_place, date_of_birth, deleted)
 values (46311425754,'Ali','Yılmaz','ayilmaz','Male','Istanbul','1999-12-05',false);
INSERT INTO user(Tc_No, Name, surname,user_name ,Gender, birth_place, date_of_birth, deleted)
 values (46365225754,'Ahmet','Yılmazer','ayılmazar','Male','Istanbul','1999-12-05',false);
