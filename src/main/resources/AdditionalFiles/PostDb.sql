#CREATE DATABASE UserDatabase;
USE post_database;

CREATE TABLE post(
  post_id bigint NOT NULL ,
  post_creator_id bigint NOT NULL ,
  category_id bigint NOT NULL ,
  permalink varchar(200) NOT NULL ,
  Title varchar(200),
  content TEXT NOT NULL ,
  date_created DATETIME NOT NULL ,
  date_updated DATETIME NOT NULL ,
  deleted boolean NOT NULL,
  is_private boolean NOT NULL ,
  like_count int NOT NULL ,
  times_viewed int NOT NULL , #Number of time viewed

  PRIMARY KEY (post_id),
  UNIQUE (permalink)

);

INSERT INTO post(post_id,category_id ,post_creator_id,permalink ,Title, content, date_created, date_updated, deleted, is_private, like_count, times_viewed)
VALUES (1,1,46312956754,'life-in-istanbul','Life in Istanbul','Of course, from the title of our main article, the first thing that may come to your mind are your questions about life in Istanbul and if Istanbul is a good place to live in?.

When you walk in the streets of Istanbul you will feel relaxed.

You will also see that people of all colors, ideas, religions live together.

From veiled women and girls to men, everywhere, in the streets, the subway and the malls…

In addition to this, you will see many handsome boys and girls with completely different ideas and prismatic appearance. But without any heavy sight or unusual confrontations.

You also find that there is enough space for every kind of faith, life, thought and style.

The feeling of freedom and calm that reigns in Istanbul makes you happy and full of energy.','2021-06-12','2021-06-12',false,false,14,44);


INSERT INTO post(post_id,category_id ,post_creator_id,permalink ,Title ,content, date_created, date_updated, deleted, is_private, like_count, times_viewed)
VALUES (2,1,46312956754,'life-in-the-city-that-connects-continents','Life in Istanbul','the first thing that may come to your mind are your questions about life in Istanbul and if Istanbul is a good place to live in?.

In addition to this, you will see many handsome boys and girls with completely different ideas and prismatic appearance. But without any heavy sight or unusual confrontations.

You also find that there is enough space for every kind of faith, life, thought and style.

The feeling of freedom and calm that reigns in Istanbul makes you happy and full of energy.','2021-06-12','2021-06-12',false,false,14,44);

