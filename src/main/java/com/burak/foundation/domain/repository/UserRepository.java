package com.burak.foundation.domain.repository;

import com.burak.foundation.domain.model.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {
    Iterable<UserEntity> findUserByTcNo(long Tc);

    Iterable<UserEntity> findUserByName(String name);

    void deleteUserByName(String name);
}
