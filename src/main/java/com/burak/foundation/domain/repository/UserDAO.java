package com.burak.foundation.domain.repository;

import com.burak.foundation.domain.model.entity.UserEntity;
import org.apache.catalina.User;

import java.util.List;

public interface UserDAO {
    List<UserEntity> getAllUsers(int pageNumber);

    List<UserEntity> getUsersByName(String name);

    UserEntity getUserByTcNo(long tc);

    List<UserEntity> getByUserSurName(String name);

    List<UserEntity> getByFullName(String name, String surname);

    UserEntity getUserByUserName(String name);

    void createUser(UserEntity user);

    void updateUser(UserEntity user);

    void UpdateUserByTc(long id, String name);

    void deleteUser(UserEntity user);

}
