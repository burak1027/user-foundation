
package com.burak.foundation.domain.repository;

import com.burak.foundation.domain.model.entity.UserEntity;
import org.apache.catalina.User;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
    private EntityManager entityManager;


    @Override
    public List<UserEntity> getAllUsers(int pageNumber) {
        Session session = entityManager.unwrap(Session.class);
//        List<UserEntity> userList = session.createQuery("FROM UserEntity").list();
        Query query = session.createQuery("FROM UserEntity");
        int numberOfItemsInAPage = 10;
        query.setFirstResult((pageNumber - 1) * numberOfItemsInAPage);
        query.setMaxResults(numberOfItemsInAPage);
        List<UserEntity> userList = query.list();
        return userList;
    }

    @Override
    public List<UserEntity> getUsersByName(String name) {
        Session session = entityManager.unwrap(Session.class);
        String hql = "From UserEntity u WHERE u.name = :userName ";
        Query query = session.createQuery(hql);
        query.setParameter("userName", name);
        List<UserEntity> users = query.list();
        session.close();
        return users;


    }

    @Override
    public UserEntity getUserByTcNo(long tc) {
        UserEntity user = null;
        Session session = entityManager.unwrap(Session.class);
        user = session.get(UserEntity.class, tc);
        session.close();
        return user;


    }

    @Override
    public List<UserEntity> getByUserSurName(String name) {
        Session session = entityManager.unwrap(Session.class);
        String hql = "From UserEntity u WHERE u.surname = :userName ";
        Query query = session.createQuery(hql);
        query.setParameter("userName", name);
        List<UserEntity> users = query.list();
        session.close();
        return users;
    }

    @Override
    public List<UserEntity> getByFullName(String name, String surname) {
        Session session = entityManager.unwrap(Session.class);
        String hql = "From UserEntity u WHERE u.name = :userName and u.surname = :userSurName";
        Query query = session.createQuery(hql);
        query.setParameter("userName", name);
        query.setParameter("userSurName",surname);
        List<UserEntity> users = query.list();
        session.close();
        return users;
    }

    @Override
    public UserEntity getUserByUserName(String name) {
        Session session = entityManager.unwrap(Session.class);
       List<UserEntity>userEntityList = (List<UserEntity>) session.
               createQuery("FROM UserEntity WHERE userName = :username")
                .setParameter("username",name).list();
        System.out.println(userEntityList.size());
        if(userEntityList.size()==0)
            return null;
        return userEntityList.get(0);
    }

    @Override
    @Transactional
    public void createUser(UserEntity user) {
        Session session = entityManager.unwrap(Session.class);
        UserEntity u = (UserEntity) session.merge(user);
        //System.out.println(u.getName());
        session.save(u);
    }

    @Override
    public void updateUser(UserEntity user) {
        Session session = entityManager.unwrap(Session.class);
        session.merge(user);
    }

    @Override
    public void UpdateUserByTc(long id, String name) {
        Session session = entityManager.unwrap(Session.class);
        UserEntity user = (UserEntity) session.get(UserEntity.class, id);
        user.setName(name);
        session.close();

    }

    @Override
    public void deleteUser(UserEntity user) {
        Session session = entityManager.unwrap(Session.class);
//        UserEntity u = (UserEntity) session.merge(user);
//        session.remove(u);
        session.remove(session.contains(user) ? user : session.merge(user));

    }


}
