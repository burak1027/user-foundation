package com.burak.foundation.domain.controller;

import com.burak.foundation.domain.model.dto.UserDto;
import com.burak.foundation.domain.service.UserLoadingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
@Api(value = "user", produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
public class UserLoadingController {

    private final UserLoadingService userLoadingService;

    @Autowired
    public UserLoadingController(UserLoadingService userLoadingService) {
        this.userLoadingService = userLoadingService;
    }

    @GetMapping("/get-all/{number}")
    public List<UserDto> getall(@Positive @PathVariable("number") int number) {
        List<UserDto> users = userLoadingService.getAllUsers(number);
        return users;
    }

    @GetMapping("/{id}")
    public UserDto getUserById(@Positive @PathVariable("id") long id) {
        //TODO implement it!
        UserDto user = userLoadingService.getUserByUserByTc(id);
        return user;
    }
    //@ApiResponse(code = 200, message = "SUCCESS", response = UserDto.class)
    @GetMapping(value = "/get-user",consumes = "application/json")
    public UserDto getUserByUserObject(@RequestBody UserDto userDto){
        System.out.println(userDto.toString());
        return userLoadingService.getUserByUserByTc(userDto.getTcNo());
    }

    @GetMapping("/name/{name}")
    public List<UserDto> listUsersByName(@Positive @PathVariable("name") String name) {
        List<UserDto> users = userLoadingService.getUsersByUserName(name);
        return users;
    }
    @GetMapping("/surname/{surname}")
    public List<UserDto> listUsersBySurname(@NotBlank @Size(min = 2) @PathVariable("surname") String surname){
        return userLoadingService.getByUserSurName(surname);
    }
    @GetMapping("/get-by-full-name")
    public List<UserDto> listUsersByFullName(@NotBlank @Size(min=2) @RequestParam String name,@NotBlank @Size(min=2) @RequestParam String surname){
        return userLoadingService.getByFullName(name,surname);
    }
    @GetMapping("/get-by-username/{name}")
    public UserDto  getUserByUserName(@PathVariable("name") String name){
        return  userLoadingService.getUserByUserName(name);
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
