package com.burak.foundation.domain.controller;

import com.burak.foundation.domain.model.dto.UserDto;
import com.burak.foundation.domain.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
@Api(value = "user", produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(consumes = "application/json")
    //@ResponseBody

    public void save(@Valid @RequestBody UserDto user) {
        System.out.println(user);
        userService.save(user);
    }

    @PutMapping(consumes = "application/json")
//    @ResponseBody
    public void update(@Valid @RequestBody UserDto user) {
        userService.save(user);
    }
    @DeleteMapping
    @ResponseBody
    public void delete(@Valid @RequestBody UserDto userDto){
        userService.deleteUser(userDto);
    }

    @DeleteMapping(value = "/delete-by-name/{name}")
//    @ResponseBody
    public void deleteUserByName(@NotBlank @PathVariable("name") String name) {
        userService.deleteUserByName(name);
    }

    @DeleteMapping(value = "/delete-by-id/{id}")
//    @ResponseBody
    public void deleteUserById(@Positive @PathVariable("id") long id) {
        userService.deleteUserById(id);

    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
