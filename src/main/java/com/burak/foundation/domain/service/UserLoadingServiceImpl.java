package com.burak.foundation.domain.service;

import com.burak.foundation.domain.model.dto.UserDto;
import com.burak.foundation.domain.model.entity.UserEntity;
import com.burak.foundation.domain.repository.UserDAOImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

//TODO
@Service
public class UserLoadingServiceImpl implements UserLoadingService {
    private final UserDAOImpl userDAO;
   // private final ModelMapper modelMapper;
    @Autowired
    public UserLoadingServiceImpl(UserDAOImpl userDAO) {
        this.userDAO = userDAO;
    }

    @Transactional(readOnly = true)
    public List<UserDto> getUsersByUserName(String name) {
        List<UserEntity> userList = userDAO.getUsersByName(name);
        List<UserDto> users = new ArrayList<>();
        userList.forEach(userEntity ->
                users.add(entityToDto(userEntity)));
        return users;

    }

    @Transactional(readOnly = true)
    public List<UserDto> getAllUsers(int pageNumber) {
        List<UserEntity> userList = userDAO.getAllUsers(pageNumber);
        List<UserDto> userDtoList = new ArrayList<>();
        userList.forEach(userEntity -> userDtoList.add(entityToDto(userEntity)));
        return userDtoList;
    }

    @Transactional(readOnly = true)
    public UserDto getUserByUserByTc(long id) {
        UserDto user = entityToDto(userDAO.getUserByTcNo(id));
        return user;

    }

    @Override
    public UserDto getUserByUserName(String userName) {
        return entityToDto(userDAO.getUserByUserName(userName));
    }

    @Transactional(readOnly = true)
    @Override
    public List<UserDto> getByUserSurName(String name) {
        List<UserDto> list = new ArrayList<>();
        userDAO.getByUserSurName(name).forEach(userEntity -> list.add(entityToDto(userEntity)));
        return list;
    }
    @Transactional(readOnly = true)
    @Override
    public List<UserDto> getByFullName(String name, String surname) {
        List<UserDto> list = new ArrayList<>();
        userDAO.getByFullName(name,surname).forEach(userEntity -> list.add(entityToDto(userEntity)));
        return list;
    }

    private UserDto entityToDto(UserEntity userEntity) {
        //UserDto userDto = modelMapper.map(userEntity, UserDto.class);
        if(userEntity==null)
            return null;
        UserDto userDto = new UserDto();
        userDto.setBirthPlace(userEntity.getBirthPlace());
        userDto.setDeleted(userEntity.isDeleted());
        userDto.setGender(userEntity.getGender());
        userDto.setName(userEntity.getName());
        userDto.setSurname(userEntity.getSurname());
        userDto.setDateOfBirth(userEntity.getDateOfBirth());
        userDto.setTcNo(userEntity.getTcNo());
        userDto.setUserName(userEntity.getUserName());
        return userDto;
    }

    private UserEntity dtoToEntity(UserDto userDto) {
        if(userDto==null)
            return null;
        //UserEntity userEntity = modelMapper.map(userDto, UserEntity.class);
        UserEntity userEntity = new UserEntity();
        userEntity.setBirthPlace(userDto.getBirthPlace());
        userEntity.setDeleted(userDto.isDeleted());
        userEntity.setGender(userDto.getGender());
        userEntity.setName(userDto.getName());
        userEntity.setSurname(userDto.getSurname());
        userEntity.setDateOfBirth(userDto.getDateOfBirth());
        userEntity.setTcNo(userDto.getTcNo());
        userEntity.setUserName(userDto.getUserName());
        return userEntity;

    }

//    private UserDto entityToDto(UserEntity userEntity) {
//        UserDto userDto = modelMapper.map(userEntity, UserDto.class);
//        return userDto;
//    }
//
//    private UserEntity dtoToEntity(UserDto userDto) {
//        UserEntity userEntity = modelMapper.map(userDto, UserEntity.class);
//        return userEntity;
//
//    }

}
