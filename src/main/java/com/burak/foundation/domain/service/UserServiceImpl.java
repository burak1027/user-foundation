package com.burak.foundation.domain.service;

import com.burak.foundation.domain.model.dto.UserDto;
import com.burak.foundation.domain.model.entity.UserEntity;
import com.burak.foundation.domain.repository.UserDAOImpl;
import com.burak.foundation.domain.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserDAOImpl userDAO;
//    private final ModelMapper modelMapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserDAOImpl userDAO) {
        this.userRepository = userRepository;
        this.userDAO = userDAO;
    }

    @Transactional
    public void save(UserDto user) {
        UserEntity userEntity = dtoToEntity(user);
        userDAO.createUser(userEntity);
    }

    @Transactional
    public void deleteUser(UserDto user) {
        //userRepository.delete(user);
        UserEntity userEntity = dtoToEntity(user);
        userDAO.deleteUser(userEntity);
    }

    @Transactional
    public void deleteUserByName(String name) {
        //userRepository.deleteUserByName(name);
        List<UserEntity> users = userDAO.getUsersByName(name);
        //users.forEach(user -> userDAO.deleteUser(user));
        for (UserEntity u : users) {
            System.out.println("---infos are ---");
            System.out.println(u.getName());
            System.out.println(u.getTcNo());
            userDAO.deleteUser(u);
        }

    }

    @Override
    @Transactional
    public void updateUser(UserDto user) {
        UserEntity userEntity = dtoToEntity(user);
        userDAO.updateUser(userEntity);

    }

    @Override
    @Transactional
    public void deleteUserById(long id) {
        UserEntity userEntity = userDAO.getUserByTcNo(id);
        userDAO.deleteUser(userEntity);
    }

    private UserDto entityToDto(UserEntity userEntity) {
        //UserDto userDto = modelMapper.map(userEntity, UserDto.class);
        if (userEntity==null)
            return null;
        UserDto userDto = new UserDto();
        userDto.setBirthPlace(userEntity.getBirthPlace());
        userDto.setDeleted(userEntity.isDeleted());
        userDto.setGender(userEntity.getGender());
        userDto.setName(userEntity.getName());
        userDto.setSurname(userEntity.getSurname());
        userDto.setDateOfBirth(userEntity.getDateOfBirth());
        userDto.setTcNo(userEntity.getTcNo());
        userDto.setUserName(userEntity.getUserName());
        return userDto;
    }

    private UserEntity dtoToEntity(UserDto userDto) {
        //UserEntity userEntity = modelMapper.map(userDto, UserEntity.class);
        if (userDto==null)
            return null;
        UserEntity userEntity = new UserEntity();
        userEntity.setBirthPlace(userDto.getBirthPlace());
        userEntity.setDeleted(userDto.isDeleted());
        userEntity.setGender(userDto.getGender());
        userEntity.setName(userDto.getName());
        userEntity.setSurname(userDto.getSurname());
        userEntity.setDateOfBirth(userDto.getDateOfBirth());
        userEntity.setTcNo(userDto.getTcNo());
        userEntity.setUserName(userDto.getUserName());
        return userEntity;

    }


}
