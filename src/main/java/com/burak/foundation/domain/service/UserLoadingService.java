package com.burak.foundation.domain.service;

import com.burak.foundation.domain.model.dto.UserDto;
import com.burak.foundation.domain.model.entity.UserEntity;

import java.util.List;

public interface UserLoadingService {
    List<UserDto> getUsersByUserName(String name);

    List<UserDto> getAllUsers(int pageNumber);

    UserDto getUserByUserByTc(long id);

    UserDto getUserByUserName(String userName);

    List<UserDto> getByUserSurName(String name);

    List<UserDto> getByFullName(String name, String surname);
}
