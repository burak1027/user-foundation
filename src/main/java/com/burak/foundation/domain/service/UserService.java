package com.burak.foundation.domain.service;

import com.burak.foundation.domain.model.dto.UserDto;
import org.springframework.stereotype.Service;

@Service
public interface UserService {

    void save(UserDto user);

    void deleteUser(UserDto user);

    void deleteUserByName(String name);

    void updateUser(UserDto user);

    void deleteUserById(long id);

}
