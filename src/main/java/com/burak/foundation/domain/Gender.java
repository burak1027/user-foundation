package com.burak.foundation.domain;

public enum Gender {
    MALE, FEMALE, TRANSGENDER, OTHER
}
