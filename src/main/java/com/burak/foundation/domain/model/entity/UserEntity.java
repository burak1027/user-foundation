package com.burak.foundation.domain.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "User")
@SQLDelete(sql = "UPDATE user SET deleted = true WHERE Tc_No=?")
@Where(clause = "deleted=false")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class UserEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Tc_No")
    private long tcNo;
    @Column(name = "user_name",unique = true)
    private String userName;
    @Column(name = "Name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Column(name = "gender")
    private String gender;
    @Column(name = "birth_place")
    private String birthPlace;
    @Column(name = "date_of_birth")
    @JsonFormat(pattern="yyyy-MM-dd")
    @Type(type = "date")
    private Date dateOfBirth;
    @Column(name = "deleted")
    private boolean deleted = Boolean.FALSE;


}
