package com.burak.foundation.domain.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.util.Date;

// TODO
// Validation + naming + private access https://www.baeldung.com/javax-validation
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class UserDto {
 // @Size(min = 11, max = 11)
 @Digits(integer = 11, fraction = 0)
 private long tcNo;
 @Size(min = 2,max = 200)
 @Pattern(regexp = "([A-Z][a-z]+\\s)*([A-Z][a-z]+)")
 private String name;
 @Size(min = 2, max = 100)
 @Pattern(regexp = "([A-Z][a-z]+\\s)*([A-Z][a-z]+)")
 private String surname;
 private String userName;

 @NotBlank
 private String Gender;
 @NotBlank
 private String birthPlace;
 //@DateTimeFormat(pattern = "yyyy/MM/dd/")
 @ApiModelProperty(required = true, dataType = "org.joda.time.LocalDate", example = "2021-05-21")
 @JsonFormat(pattern="yyyy-MM-dd")
 @Past
 private Date dateOfBirth;
 @AssertFalse
 private boolean deleted = Boolean.FALSE;


}
