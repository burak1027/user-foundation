package com.burak.foundation;

import com.burak.foundation.domain.model.dto.UserDto;
import com.burak.foundation.domain.service.UserLoadingService;
import com.burak.foundation.domain.service.UserService;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestPropertySource(locations= "classpath:application-test.yml")
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class UserServiceTest {

    UserService userService;
    UserLoadingService userLoadingService;

    @Autowired
    public UserServiceTest(UserService userService, UserLoadingService userLoadingService) {
        this.userService = userService;
        this.userLoadingService = userLoadingService;
    }
    void printAllUsers(){
        userLoadingService.getAllUsers(1).forEach(userDto -> System.out.println(userDto.toString()));
        userLoadingService.getAllUsers(2).forEach(userDto -> System.out.println(userDto.toString()));

    }
    @Order(1)
    @Test
    void createUsers(){
        for (int i = 1; i < 11; i++) {
            UserDto userDto;
            if(i%3==0){
                userDto= UserDto.builder().tcNo(15352654653L+i).name("Burak").build();
            }
            else if( i%3 == 1){
                userDto= UserDto.builder().tcNo(15352654653L+i).name("Ali").build();

            }
            else{
                userDto= UserDto.builder().tcNo(15352654653L+i).name("Ahmet").build();

            }
            userService.save(userDto);
        }
    }
    @Order(2)
    @Test
    void getUsersByNameTest(){
        userLoadingService.getUsersByUserName("Burak").forEach(userDto -> System.out.println(userDto.toString()));
    }
    @Order(3)
    @Test
    void getUserByTc(){
        System.out.println(userLoadingService.getUserByUserByTc(15352654653L+3).toString());
    }
    @Order(4)
    @Test
    void deleteByName(){
        userService.deleteUserByName("Burak");
        System.out.println("---ALL USERS---");
        printAllUsers();
    }
    @Order(5)
    @Test
    void getUserByTcAnddeleteByTc(){
        System.out.println(userLoadingService.getUserByUserByTc(15352654657L).toString());
        userService.deleteUserById(15352654657L);
//        System.out.println(userLoadingService.getUserByUserByTc(15352654657L));
        assertEquals(userLoadingService.getUserByUserByTc(15352654657L),null);

    }
    @Order(6)
    @Test
    void printUsers(){
        printAllUsers();
    }


}
