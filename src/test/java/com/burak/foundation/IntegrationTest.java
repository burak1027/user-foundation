package com.burak.foundation;

import com.burak.foundation.domain.model.dto.UserDto;
import com.burak.foundation.domain.service.UserLoadingService;
import com.burak.foundation.domain.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.parseMediaType;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
//@TestPropertySource("classpath:application-test.properties")
@TestPropertySource(locations= "classpath:application-test.yml")
@ActiveProfiles("test")
@AutoConfigureMockMvc
@WebAppConfiguration
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class IntegrationTest {
    MockMvc mockMvc;
    UserService userService;
    UserLoadingService userLoadingService;
    @Autowired
    public IntegrationTest(MockMvc mockMvc, UserService userService, UserLoadingService userLoadingService) {
        this.mockMvc = mockMvc;
        this.userService = userService;
        this.userLoadingService = userLoadingService;
    }

    public  String asJsonString(UserDto userDto) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(userDto);
        return requestJson;

    }
    @Order(1)
    @Test
    void createUsers(){
        for (int i = 1; i < 11; i++) {
            UserDto userDto;
            if(i%3==0){
                userDto= UserDto.builder().tcNo(15352654653L+i).name("Tolga").surname("Beser").userName("tbeser"+i).build();
            }
            else if( i%3 == 1){
                userDto= UserDto.builder().tcNo(15352654653L+i).name("Ali").surname("Saglam").userName("asaglam"+i).build();

            }
            else{
                userDto= UserDto.builder().tcNo(15352654653L+i).name("Umut").surname("Bayrak").userName("ubayrak"+i).build();

            }
            System.out.println(userDto);
            userService.save(userDto);
        }
    }
    @Order(2)
    @Test
    void getByIdMockMvcTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/15352654656"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(APPLICATION_JSON));
    }
    @Order(3)
    @Test
    void getByNameMockMvcTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/name/Ali"))
                .andDo(print()).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(APPLICATION_JSON));
    }
    @Order(4)
    @Test
    void getBySurname() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/user/surname/Bayrak")).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(APPLICATION_JSON));
    }
    @Order(5)
    @Test
    void getByFullNameTest() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/user//get-by-full-name").param("name","Tolga").param("surname","Beser"))
                .andDo(print());
    }
    @Order(6)
    @Test
    void createUser() throws Exception {
       UserDto userDto= UserDto.builder().tcNo(12940129402L).name("Burak").surname("Gunden").Gender("Male").birthPlace("Istanbul").userName("sphyre").build();
        mockMvc.perform(MockMvcRequestBuilders.post("/user").content(asJsonString(userDto)).contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        assertEquals(userLoadingService.getUserByUserByTc(12940129402L).getName(),"Burak");
    }
    @Order(7)
    @Test
    void updateUser() throws Exception{
        UserDto userDto= UserDto.builder().tcNo(12940129402L).name("Burak Bahir").surname("Gunden").Gender("Male").birthPlace("Istanbul").userName("sphyre").build();
        mockMvc.perform(MockMvcRequestBuilders.put("/user").content(asJsonString(userDto)).contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        assertNotEquals(userLoadingService.getUserByUserByTc(12940129402L).getName(),"Burak");
    }
    @Order(8)
    @Test
    void deleteUser() throws Exception{
        UserDto userDto= UserDto.builder().tcNo(12940129402L).name("Burak Bahir").surname("Gunden").Gender("Male").birthPlace("Istanbul").build();
        mockMvc.perform(MockMvcRequestBuilders.delete("/user").content(asJsonString(userDto)).contentType(APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        assertNull(userLoadingService.getUserByUserByTc(12940129402L));
    }
    @Order(9)
    @Test
    void getUserByUsername() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/get-by-username/tbeser9"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
