package com.burak.foundation;

import com.burak.foundation.domain.model.dto.UserDto;
import com.burak.foundation.domain.model.entity.UserEntity;
import com.google.common.base.Stopwatch;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class BeanMapperTest {


    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(10);

    @Test
    public void test1() throws ExecutionException, InterruptedException {
        Stopwatch timer = Stopwatch.createStarted();
        ModelMapper modelMapper = new ModelMapper();
        AtomicInteger atomicInteger = new AtomicInteger();
        List<Future<Boolean>> featureList = new ArrayList<>();
        for (int i = 0; i < 500000; i++) {
            Future<Boolean> submit = EXECUTOR_SERVICE.submit(getBooleanCallable(modelMapper, atomicInteger));
            featureList.add(submit);
        }
        for (Future<Boolean> feature : featureList) {
            feature.get();
        }
        System.out.println(timer.elapsed(TimeUnit.MILLISECONDS));
    }

    private Callable<Boolean> getBooleanCallable(ModelMapper modelMapper,
                                                 AtomicInteger atomicInteger) {
        return () -> {
            int iterationCount = atomicInteger.incrementAndGet();
            if (iterationCount % 100 == 0) {
                System.out.println("Iteration Count = " + iterationCount);
            }
            modelMapper.map(createEntity(), UserDto.class);
            return true;
        };
    }

    private UserEntity createEntity() {
        return UserEntity.builder().name("Burak").birthPlace("GOP").gender("M").surname("Gunden").build();
    }
}
